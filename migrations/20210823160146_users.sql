-- Add migration script here
CREATE TABLE USERS (
    username TEXT NOT NULL PRIMARY KEY,
    password TEXT NOT NULL,
    rsa_key BYTEA NOT NULL
);

CREATE TABLE GPS (
    sender TEXT NOT NULL,
    reciever TEXT NOT NULL,
    longitude BYTEA,
    latitude BYTEA,
    moddate TIMESTAMPTZ,
    PRIMARY KEY (sender, reciever)
);

CREATE TABLE SESSIONS (
    uid TEXT PRIMARY KEY,
    timestamp TIMESTAMPTZ NOT NULL,
    username TEXT NOT NULL
);
