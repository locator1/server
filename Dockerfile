###########
## Build ##
###########

FROM ekidd/rust-musl-builder:1.57.0 as builder
USER root
WORKDIR /app

# Copy over the directory
COPY . .

# Build the program
RUN cargo build --release --target x86_64-unknown-linux-musl

# Remove debug symbols
RUN strip /app/target/x86_64-unknown-linux-musl/release/locator-server

############
## Deploy ##
############

FROM alpine

# Install dependencies
USER root
COPY --from=builder /app/target/x86_64-unknown-linux-musl/release/locator-server /usr/local/bin/

CMD ["/usr/local/bin/locator-server"]
