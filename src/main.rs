#![warn(clippy::pedantic)]
#![allow(
    clippy::missing_errors_doc,
    clippy::missing_panics_doc,
    clippy::module_name_repetitions
)]

use rocket::http::Method;
use rocket::Request;
use rocket::State;
use sqlx::postgres::{PgPool, PgPoolOptions};
use tokio::sync::RwLock;
use serde_json::{json, Value};
use rocket::serde::json::Json;

pub mod cors;
pub mod gps;
pub mod login;
pub mod util;

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate log;

pub type DB = State<RwLock<PgPool>>;

#[derive(Responder)]
pub enum Catch404 {
    #[response(status = 200)]
    Ok(()),
    #[response(status = 404, content_type = "json")]
    Json(Json<Value>),
}

#[catch(404)]
fn catcher_404(req: &Request) -> Catch404 {
    if req.method() == Method::Options {
        log::debug!("Browser requested options");
        Catch404::Ok(())
    } else {
        Catch404::Json(Json(json!({
            "error": {
                "code": 404,
                "reason": "Not Found",
                "description": "Not Found",
            }
        })))
    }
}

#[launch]
async fn rocket() -> _ {
    dotenv::dotenv().ok();

    let conn = std::env::var("DATABASE_URL").expect("Database url not set");
    let conn = PgPoolOptions::new().connect(&conn).await.expect("Failed to connect to database");

    sqlx::migrate!()
        .run(&conn)
        .await
        .expect("Failed to run migrations");

    env_logger::init();
    rocket::build()
        .manage(RwLock::new(conn))
        .attach(cors::CORS)
        .register("/", catchers![catcher_404])
        .mount(
            "/",
            routes![
                login::create_account,
                login::login,
                login::get_rsa_key,
                login::reset_rsa_key,
                login::delete_login,
                gps::get,
                gps::create_connection,
                gps::get_connections,
                gps::update_gps,
                gps::delete_connection_by_reciever,
                gps::delete_connection_by_sender,
            ],
        )
}
