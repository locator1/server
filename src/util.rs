use std::fmt::Display;

use rocket::http::Status;

// Trait to just log errors and return an InternalServerError.
// Not really a trait though.
pub(crate) trait LogServerError {
    type Ok;
    fn log_err(self) -> Result<Self::Ok, Status>;
}

impl<T, E: Display> LogServerError for Result<T, E> {
    type Ok = T;

    fn log_err(self) -> Result<T, Status> {
        self.map_err(|e| {
            error!("{}", e);
            Status::InternalServerError
        })
    }
}
