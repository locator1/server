use chrono::prelude::*;
use rocket::http::Status;
use rocket::serde::{json::Json, Deserialize, Serialize};

use crate::login::get_user_from_session;
use crate::util::LogServerError;
use crate::DB;

#[derive(Serialize, Deserialize)]
pub struct Gps {
    pub longitude: Option<Vec<u8>>,
    pub latitude: Option<Vec<u8>>,
    pub date: Option<chrono::DateTime<Utc>>,
}

#[get("/gps/by_sender/<sender>?<session>")]
pub async fn get(
    conn: &DB,
    sender: &str,
    session: &str,
) -> Result<Json<Gps>, Status> {
    let db = conn.read().await;

    let query = sqlx::query!(
        "SELECT
            longitude,
            latitude,
            moddate
        FROM
            GPS
        WHERE
            sender=$1 AND
            reciever=$2",
        sender,
        get_user_from_session(&*db, session).await?,
    )
    .fetch_optional(&*db)
    .await
    .log_err()?
    .ok_or(Status::NotFound)?;

    Ok(Json(Gps {
        longitude: query.longitude,
        latitude: query.latitude,
        date: query.moddate,
    }))
}

/// Creates a new connection between the sender (who sent the request)
/// and the reciever, who the sender has decided to share GPS information
/// with.
#[post("/gps?<session>", data = "<data>")]
pub async fn create_connection(
    conn: &DB,
    session: &str,
    data: Json<&str>,
) -> Result<Status, Status> {
    let db = conn.read().await;
    let user = get_user_from_session(&*db, session).await?;

    tokio::try_join!(
        async {
            // Checks that this connection could be made
            if sqlx::query!("SELECT FROM USERS WHERE username=$1", &*data)
                .fetch_optional(&*db)
                .await
                .log_err()?
                .is_none()
            {
                Err(Status::NotFound)
            } else {
                Ok(())
            }
        },
        async {
            // Checks that this connection will not conflict
            if sqlx::query!("SELECT FROM gps WHERE sender=$1 AND reciever=$2", user, &*data)
                .fetch_optional(&*db)
                .await
                .log_err()?
                .is_some()
            {
                Err(Status::Conflict)
            } else {
                Ok(())
            }
        }
    )?;

    // Insert the new connection.
    sqlx::query!(
        "
        INSERT INTO GPS
        (sender, reciever)
        VALUES ($1, $2)
        ",
        user,
        &*data,
    )
    .execute(&*db)
    .await
    .log_err()?;

    Ok(Status::Created)
}

#[derive(Serialize)]
pub struct ListConnectionsItem {
    pub reciever: String,
    pub sender: String,
}

/// Returns a list of all a users' connections, with or without the GPS
/// information.
#[get("/gps?<session>")]
pub async fn get_connections(
    conn: &DB,
    session: &str,
) -> Result<Json<Vec<ListConnectionsItem>>, Status> {
    let db = conn.read().await;

    let rows = sqlx::query!(
        "SELECT
            sender,
            reciever
        FROM
            GPS
        WHERE
            sender = $1 OR
            reciever = $1
        ",
        get_user_from_session(&*db, session).await?,
    )
    .fetch_all(&*db)
    .await
    .log_err()?;

    let mut vec = Vec::with_capacity(rows.len());

    for x in rows {
        vec.push(ListConnectionsItem {
            reciever: x.reciever,
            sender: x.sender,
        });
    }

    Ok(Json(vec))
}

/// Lets you send your gps coordinates to the server to a given reciever.
#[put("/gps/by_reciever/<reciever>?<session>", data = "<gps>")]
pub async fn update_gps(
    conn: &DB,
    session: &str,
    reciever: &str,
    gps: Json<Gps>,
) -> Result<Status, Status> {
    let db = conn.read().await;

    sqlx::query!(
        "UPDATE
            GPS
        SET
            longitude = $1,
            latitude = $2,
            moddate = $3
        WHERE
            reciever = $4 AND
            sender = $5
        RETURNING longitude;
        ",
        gps.longitude,
        gps.latitude,
        gps.date,
        reciever,
        get_user_from_session(&*db, session).await?,
    )
    .fetch_optional(&*db)
    .await
    .log_err()?
    .ok_or(Status::NotFound)
    .map(|_| Status::Ok)
}

/// Lets one delete a connection, they can be either the reciever or sender.
#[delete("/gps/by_reciever/<reciever>?<session>")]
pub async fn delete_connection_by_reciever(
    conn: &DB,
    session: &str,
    reciever: &str,
) -> Result<Status, Status> {
    let db = conn.read().await;

    sqlx::query!(
        "DELETE FROM
            GPS
        WHERE
            reciever = $1 AND sender = $2
        RETURNING longitude;
        ",
        reciever,
        get_user_from_session(&*db, session).await?,
    )
    .fetch_optional(&*db)
    .await
    .log_err()?
    .ok_or(Status::NotFound)
    .map(|_| Status::NoContent)
}

/// Lets one delete a connection, they can be either the reciever or sender.
#[delete("/gps/by_sender/<sender>?<session>")]
pub async fn delete_connection_by_sender(
    conn: &DB,
    session: &str,
    sender: &str,
) -> Result<Status, Status> {
    let db = conn.read().await;

    sqlx::query!(
        "DELETE FROM
            GPS
        WHERE
            sender = $1 AND reciever = $2
        RETURNING longitude;
        ",
        sender,
        get_user_from_session(&*db, session).await?,
    )
    .fetch_optional(&*db)
    .await
    .log_err()?
    .ok_or(Status::NotFound)
    .map(|_| Status::NoContent)
}
