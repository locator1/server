use rand::prelude::*;
use rocket::http::Status;
use rocket::serde::{json::Json, Deserialize};

use crate::util::LogServerError;
use crate::DB;

// Generates a salt
#[must_use = "randomness is expensive"]
pub fn salt() -> [u8; 32] {
    rand::thread_rng().gen()
}

#[derive(Deserialize)]
pub struct LoginUser<'a> {
    pub username: &'a str,
    pub password: &'a str,
    pub rsa_key: Vec<u8>,
}

#[derive(Deserialize)]
pub struct Login<'a> {
    pub username: &'a str,
    pub password: &'a str,
}

#[post("/login/users", data = "<user>", format = "application/json")]
pub async fn create_account(
    conn: &DB,
    user: Json<LoginUser<'_>>,
) -> Result<Status, Status> {
    let db = &*conn.read().await;

    if sqlx::query!("SELECT FROM USERS WHERE username=$1", user.username)
        .fetch_optional(&*db)
        .await
        .log_err()?
        .is_some()
    {
        return Err(Status::Conflict);
    }

    sqlx::query!(
        "INSERT INTO USERS
        (username, password, rsa_key)
        VALUES
        ($1, $2, $3);
        ",
        user.username,
        argon2::hash_encoded(
            user.password.as_bytes(),
            &salt(),
            &argon2::Config::default()
        )
        .map_err(|e| match e {
            argon2::Error::OutputTooShort |
            argon2::Error::OutputTooLong |
            argon2::Error::PwdTooShort |
            argon2::Error::PwdTooLong |
            argon2::Error::SaltTooShort |
            argon2::Error::SaltTooLong => {
                Status::BadRequest
            }
            _ => {
                error!("{}", e);
                Status::InternalServerError
            }
        })?,
        user.rsa_key
    )
    .execute(db)
    .await
    .log_err()?;

    Ok(Status::Created)
}

#[post("/login/login", data = "<user>", format = "application/json")]
pub async fn login(
    conn: &DB,
    user: Json<Login<'_>>,
) -> Result<(Status, Json<String>), Status> {
    let db = &*conn.read().await;

    // Tries to get the password from the database and verify it.
    let password = sqlx::query!(
        "SELECT password
        FROM USERS
        WHERE username=$1",
        user.username,
    )
    .fetch_optional(db)
    .await
    .log_err()?
    .ok_or(Status::Forbidden)?;

    // Checks it
    if argon2::verify_encoded(&password.password, user.password.as_bytes())
        .unwrap_or(false)
    {
        // Add the timestamp.
        let uid = uuid::Uuid::new_v4().to_string();
        sqlx::query!(
            "INSERT INTO sessions
            (timestamp, uid, username)
            VALUES
            ($1, $2, $3);",
            chrono::offset::Utc::now(),
            uid,
            user.username,
        )
        .execute(db)
        .await
        .log_err()?;
        Ok((Status::Created, Json(uid)))
    } else {
        Err(Status::Forbidden)
    }
}

/// Allows one to get another user's rsa_key
#[get("/login/users/<user>/rsa_key")]
pub async fn get_rsa_key(
    conn: &DB,
    user: &str,
) -> Result<Json<Vec<u8>>, Status> {
    let db = &*conn.read().await;

    Ok(Json(
        sqlx::query!(
            "SELECT
            rsa_key
        FROM
            users
        WHERE
            username=$1",
            user
        )
        .fetch_optional(db)
        .await
        .log_err()?
        .ok_or(Status::NotFound)?
        .rsa_key,
    ))
}

/// Gets a user from a session key.
///
/// On failure shall return a 401 Unauthorised.
pub async fn get_user_from_session<'a, DB>(
    conn: DB,
    session: &str,
) -> Result<String, Status>
where
    DB: sqlx::Executor<'a, Database = sqlx::Postgres> + Copy,
{
    // Delete all the expired sessions.
    sqlx::query!(
        "
        DELETE FROM
            sessions
        WHERE
            timestamp < (NOW() - INTERVAL '1 DAYS');"
    )
    .execute(conn)
    .await
    .log_err()?;

    // Select the session.
    sqlx::query!(
        "
        SELECT
            username
        FROM
            sessions
        WHERE
            uid = $1;
        ",
        session
    )
    .fetch_optional(conn)
    .await
    .log_err()?
    .ok_or(Status::Unauthorized)
    .map(|x| x.username)
}

#[put("/login/users/<user>/rsa_key?<session>", data = "<rsa_key>")]
pub async fn reset_rsa_key(
    conn: &DB,
    user: &str,
    rsa_key: Json<Vec<u8>>,
    session: &str,
) -> Result<Status, Status> {
    let db = &*conn.read().await;
    let session_user = get_user_from_session(db, session).await?;

    if session_user == user {
        sqlx::query!(
            "UPDATE
                GPS
            SET
                longitude=NULL,
                latitude=NULL
            WHERE
                reciever=$1
            ",
            user,
        )
        .execute(db)
        .await
        .log_err()?;

        sqlx::query!(
            "UPDATE
                users
             SET
                rsa_key=$1
             WHERE
                 username=$2
             RETURNING
                 username
             ",
            &*rsa_key,
            user
        )
        .fetch_optional(db)
        .await
        .log_err()?
        .ok_or(Status::NotFound)
        .map(|_| Status::NoContent)
    } else {
        Err(Status::Forbidden)
    }
}

#[delete("/login/users?<session>")]
pub async fn delete_login(conn: &DB, session: &str) -> Result<Status, Status> {
    let db = &*conn.read().await;
    let user = get_user_from_session(db, session).await?;

    sqlx::query!(
        "DELETE FROM
            users
        WHERE
            username=$1
        RETURNING
            username",
        user,
    )
    .fetch_optional(db)
    .await
    .log_err()?
    .ok_or(Status::NotFound)?;

    sqlx::query!(
        "DELETE FROM
            gps
        WHERE
            sender=$1 OR reciever=$1",
        user,
    )
    .execute(db)
    .await
    .log_err()?;

    Ok(Status::NoContent)
}
